using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
  
public class angie : MonoBehaviour
{
    public Rigidbody2D rb;
    Animator anim;
    public float moveSpeed,range,initialMoveSpeed;
    public Transform enemies;
    public bool canAttack,isAttacking;

    void Start(){
        rb = GetComponent<Rigidbody2D>();
        anim = gameObject.GetComponent<Animator>();
        initialMoveSpeed=moveSpeed;
    }

    void Update () 
    {
        if(Math.Abs(Math.Abs(enemies.position.x)-Math.Abs(transform.position.x))<=range){
            // moveSpeed=0;
        }
        Debug.Log(transform.position.x +" "+enemies.position.x);
        SetAnimationState();
    }

    void run(){
        transform.Translate(Vector2.left * moveSpeed * Time.deltaTime);
    }

    void SetAnimationState(){
        if(moveSpeed!=0){
            anim.SetBool("isRunning",true);
            run();
        }
        // if(moveSpeed==0){
        //     anim.SetBool("isIdle",true);
        // }
    }

}