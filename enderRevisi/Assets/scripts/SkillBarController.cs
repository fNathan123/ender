using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

public class SkillBarController : MonoBehaviour
{
    public Image imageCooldown;
    public float cooldown=10f;
    bool isCooldown;
    public Button button;

    float timer;
    float seconds;

    void Start(){
        button.onClick.AddListener(() =>action());
    }

    void action(){
        isCooldown=true;
    }

    void Update(){
        if(isCooldown){
            // imageCooldown.fillAmount-=1/cooldown*Time.deltaTime;
            // imageCooldown.fillAmount=0;
            button.interactable=false;
            timerUpdate();
        }
        if(Mathf.Floor(seconds)==cooldown){
            isCooldown=false;
            timer=0;
            seconds=0;
            button.interactable=true;
        }
    }

    void timerUpdate(){
        timer += Time.deltaTime;
        seconds = timer % 60;
    }

}