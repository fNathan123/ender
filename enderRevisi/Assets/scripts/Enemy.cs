using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    // Start is called before the first frame update
    protected bool isNearPlayer;
    protected bool isWalking;
    protected bool isFighting;
    protected bool isHurt;
    protected bool isDead;
    protected bool isIddle;

    protected float hp;

    public Animator anim;

    public float getXPosition(){
        Vector3 transform = this.transform.position;
        return transform.x;
    }

    public Vector3 getPosition(){
        return transform.position;
    }

    public bool getIsNearPlayer(){
        return isNearPlayer;
    }

    public float getHp(){
        return hp;
    }

    public void setIsNearPlayer(bool value){
        isNearPlayer=value;
        isFighting=true;
    }

    public void attacked(int damage){
        hp-=damage;
    }

    public void setIsDead(bool value){
        isDead=value;
    }
    public void setIsFighting(bool value){
        isFighting=value;
    }


    void Start()
    {

    }

    public void SetAnimationState(){
        if(isWalking==true){
            anim.SetBool("isWalking",true);
        }
        if(isFighting){
            anim.SetBool("isWalking",false);
            anim.SetBool("isIddle",false);
            anim.SetBool("isAttack",true);
            if(isHurt)anim.SetBool("isHurt",true);
            // anim.SetBool("isAttack",false);
        }
        if(isDead){
            anim.SetBool("isDead",true);
        }
        // anim.SetBool("isRunning",isRunning);
        // anim.SetBool("isIddle",is)
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        if(col.gameObject.tag=="Rhino"){
            hp=0;
        };
        if (col.gameObject.tag == "Enemy") {
            // Physics.IgnoreCollision(collision.collider, collider);
            Physics2D.IgnoreCollision(col.collider,this.GetComponent<Collider2D>());
        }
    }

}
