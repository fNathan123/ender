﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Angie : MonoBehaviour
{
    bool isRunning;
    bool isFighting;
    bool isHurt;
    bool isDead;
    bool isIddle;

    bool attacking;
    bool finished;

    int attackPlayer;
    float def;
    float hp;
    int maxHp;
    Animator anim;

    GameObject healthBar;
    GameObject attackPoint;
    GameObject defPoint;
    GameObject hPoint;

    static Angie Instance;

    // Start is called before the first frame update
    void Awake(){
        Instance=this;
        maxHp=20;
        hp=(float)maxHp;
        healthBar=Controller.getInstance().healthBar;
        attackPoint=Controller.getInstance().attackPoint;
        defPoint=Controller.getInstance().defPoint;
        hPoint=Controller.getInstance().hPoint;
    }

    public static Angie getInstance(){
        // Debug.Log("HEY!");
        return Instance;
    }

    public void setIsFighting(bool value){
        isFighting=value;
        isIddle=!value;
        isRunning=!value;
    }

    public float getXPosition(){
        Vector3 transform = this.transform.position;
        return transform.x;
    }
    public bool getIsFighting(){
        return isFighting;
    }
    public int getAttack(){
        return attackPlayer;
    }

    void Start()
    {
        isRunning=true;
        isFighting=false;
        isHurt=false;
        isDead=false;
        anim = GetComponent<Animator>();
        attackPlayer=1;
        def=0;

        attacking=false;
        finished=false;

        hPoint.GetComponent<Text>().text=(int)hp+"/"+maxHp;
        attackPoint.GetComponent<Text>().text=attackPlayer+"";
        defPoint.GetComponent<Text>().text=def+"";

        // anim.SetBool("isRunning",true);
        // anim.SetBool("isIddle",false);
    }

    // Update is called once per frame
    void Update()
    {
        SetAnimationState();
        if(isFighting)attackingChecker();
        if(hp<=0){
            SceneManager.LoadScene("gameover");
        }
        // Debug.Log(anim.GetCurrentAnimatorStateInfo(0).IsName("Attack"));


        // Vector3 position = transform.position;
        // position.x -= 2*Time.deltaTime;
        // transform.position = position;
    }
    void attackingChecker(){
        if(anim.GetCurrentAnimatorStateInfo(0).IsName("Attack")){
            attacking=true;
            // sign=true;
            // Debug.Log("msk");
            // Controller.getInstance().playerAttack(attack);
        }
        if(!anim.GetCurrentAnimatorStateInfo(0).IsName("Attack") && attacking){
            finished=true;
        }
        if(finished){
            // Debug.Log("msk");
            attacking=false;
            finished=false;
            Controller.getInstance().playerAttack(attackPlayer);
        }
    }

    void SetAnimationState(){
        // if(isRunning==true){
        //     anim.SetBool("isRunning",true);
        // }
        // if(isFighting){
        //     anim.SetBool("isRunning",false);
        //     anim.SetBool("isAttack",true);
        // }
        anim.SetBool("isRunning",isRunning);
        anim.SetBool("isAttack",isFighting);

    }

    public void attacked(float attack){
        float damage=attack-def;
        if(damage<0)damage=attack/def;
        hp-=damage;
        // Debug.Log((float)hp);
        healthBar.GetComponent<Image>().fillAmount=hp/(float)maxHp;
    }

    public void addMaxHp(int hp){
        maxHp+=hp;
    }

    public void updateStat(int[] stats){
        for(int i=0;i<stats.Length;i++){
            if(stats[i]==0)attackPlayer++;
            else if(stats[i]==1)def++;
            else {
                hp+=(float)maxHp*0.3f;
                maxHp+=3;
                if(hp>(float)maxHp)hp=(float)maxHp;
                healthBar.GetComponent<Image>().fillAmount=hp/(float)maxHp;
            }
        }
        hPoint.GetComponent<Text>().text=(int)hp+"/"+maxHp;
        attackPoint.GetComponent<Text>().text=attackPlayer+"";
        defPoint.GetComponent<Text>().text=def+"";
    }

}
