﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PlayerManager : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {

    }

    public void Spawn(float x,float y){
        //CARA BIKIN OBJECT DARI PREFAB
        //-9.95 -0.9
        GameObject angie = Instantiate(Resources.Load("prefabs/heroes/Angie/Angie",typeof(GameObject))) as GameObject;
        Vector3 transform = angie.transform.position;
        transform.x = x;
        transform.y = y;
        angie.transform.position = transform;
        angie.transform.SetParent(this.transform);
        angie.AddComponent<Angie>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }


}
