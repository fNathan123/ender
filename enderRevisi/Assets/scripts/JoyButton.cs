using UnityEngine;
using UnityEngine.EventSystems;

public class JoyButton : MonoBehaviour, IPointerUpHandler, IPointerDownHandler {

    [HideInInspector]
    public bool pressed;
    public int id;

    void Start(){

    }
    
    void Update(){

    }

    public void OnPointerDown(PointerEventData eventData){
        pressed=true;
    }

    public void OnPointerUp(PointerEventData eventData){
        pressed=false;
    }
    


}