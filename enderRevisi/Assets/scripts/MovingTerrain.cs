﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingTerrain : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        // Debug.Log(Angie.getInstance().getXPosition());
        float xAngie= Angie.getInstance().getXPosition();
        float xEnemy=0; 
        GameObject enemy=null;
        if(EnemyManager.getInstance().getOrderEnemySize()!=0){
            xEnemy= EnemyManager.getInstance().getFrontEnemy().transform.position.x;
            enemy=EnemyManager.getInstance().getFrontEnemy();
        }
        Vector3 position = transform.position;
        if(enemy!=null && Mathf.Abs(xAngie - xEnemy)>10 || xEnemy==0)position.x -= 2*Time.deltaTime;
        else{
            position.x -= 0;
            Angie.getInstance().setIsFighting(true);
        }
        // Debug.Log(Mathf.Abs(xAngie - xEnemy));
        transform.position = position;
    }
}
