﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Controller : MonoBehaviour
{

    public TerrainManager terrainManager;
    public PlayerManager playerManager;
    public EnemyManager enemyManager;
    public DialogManager dialogManager;

    public float timer;
    public float timer2;
    static Controller Instance;
    int idPressed;
    bool isReleased;

    public Canvas mcanvas;
    protected Button[] buttons;
    int[] playerAnswer;
    int numClick;

    public GameObject rhino;

    public int maxSpawn;
    int spawn;
    public int curLevel;
    int kill;



    int combo;
    public GameObject comboText;
    public GameObject healthBar;
    public GameObject attackPoint;
    public GameObject defPoint;
    public GameObject hPoint;

    public static Controller getInstance(){
        return Instance;
    }

    void Awake(){
        Instance = this;
        curLevel = 0;
        maxSpawn = 3;
        spawn=0;
        combo=0;
        terrainManager.generateTerrain(-10.48722f,-1.488937f);
        playerManager.Spawn(-8f,-0.9f);
        enemyManager.Spawn(10.27f,1.82f);

        playerAnswer=new int[3];
        numClick=0;

        buttons=mcanvas.GetComponentsInChildren<Button>();
        isReleased=false;
        foreach(Button button in buttons){
           int id= button.GetComponent<JoyButton>().id;
           button.onClick.AddListener(() =>buttonAction(id));
        }
        for(int i=0;i<playerAnswer.Length;i++){
            playerAnswer[i]=-1;
        }
        //dialog bar y=3.2
    }
    void buttonAction(int id){
        // Debug.Log(id);
        if(id!=3){
            if(numClick<3){
                playerAnswer[numClick]=id;
            }
            if(numClick>=3){
                for(int i=0;i<playerAnswer.Length-1;i++){
                    playerAnswer[i]=playerAnswer[i+1];
                }
                playerAnswer[playerAnswer.Length-1]=id;
            }
            notifyDialogChange();
        }
        else{
            int[] trueAnswer = DialogManager.getInstance().getAnswer();
            bool status=true;
            // for(int i=0;i<playerAnswer.Length;i++){
            //     if(trueAnswer[i]!=playerAnswer[i])status=false;
            // }
            addCombo();
            Angie.getInstance().updateStat(playerAnswer);
            // Debug.Log(status);
            if(status){
                // playerAttack(Angie.getInstance().getAttack()*3);
                if(combo%7==0){
                    rhino = Instantiate(Resources.Load("prefabs/heroes/Rhino/Rhino",typeof(GameObject))) as GameObject;
                    Vector3 rhinoPos= new Vector3(-13.24f,0.63f,0);
                    // Rigidbody2D rhinoRb=rhino.AddComponent<Rigidbody2D>() as Rigidbody2D;
                    rhino.AddComponent<BoxCollider2D>();
                    rhino.AddComponent<Rhino>();
                    rhino.tag="Rhino";
                    rhino.GetComponent<SpriteRenderer>().sortingOrder=1;
                    rhino.transform.position=rhinoPos;
                    // rhinoRb.velocity=new Vector3(10,0,0);
                }
                
            }
        }
        numClick++;
    }

    // Start is called before the first frame update
    void Start()
    {
        timer = 0;
    }

    // Update is called once per frame
    void Update()
    {
        // if(Mathf.Abs(xAngie - xEnemy)<=2)SwordEnemy.getInstance().setIsNearPlayer(true);
        if(rhino!=null){
            Vector3 vec=rhino.transform.position;
            vec.x +=4*Time.deltaTime;
            rhino.transform.position=vec;
            if(rhino.transform.position.x>=12.8f){
                Destroy(rhino);
            }
        }
        timer += Time.deltaTime;
        if(timer>=2 && spawn < maxSpawn-1){
            timer = 0;
            spawn++;
            enemyManager.Spawn(10.27f,1.82f);
        }
        if(kill>=maxSpawn){
            timer2+=Time.deltaTime;
            if(timer2>=5){
                curLevel++;
                timer=0;
                spawn=0;
                timer2=0;
                maxSpawn+=2;
                kill=1;
            }
            // enemyLevelUp();
        }
        
        // if(joyButton.pressed)Debug.Log(joyButton.id);
        // Debug.Log(buttons.Length);

        // foreach(JoyButton button in buttons){
        //     // JoyButton buttConv = button.GetComponent<JoyButton>();
        //     idPressed=button.id;
        //     isReleased=!buttons[idPressed].pressed;
        //     if(button.pressed && !isReleased){
        //         Debug.Log(button.id);
        //     }
        // }

        // Player player = playerManager.GetComponentInChildren<Player>();
        // if(player.transform.position.x <-3){
        //     player.transform.position = Vector3.zero;
        // }

    }

    public void playerAttack(int attack){
        // SwordEnemy.getInstance().attacked(attack);
        SwordEnemy temp=EnemyManager.getInstance().getFrontEnemy().GetComponent<SwordEnemy>();
        temp.attacked(attack);
        // Debug.Log(temp.getHp());
    }

    public void enemyAttack(float attack){
        Angie.getInstance().attacked(attack);
    }

    public void notifyPlayer(){
        Angie.getInstance().setIsFighting(false);
    }

    public void notifyDead(){
        // DialogManager.getInstance().destroy();
        kill++;
    }

    public void notifyDialogChange(){
        DialogManager.getInstance().updateDialog(playerAnswer);
    }

    public void addCombo(){
        combo++;
        comboText.GetComponent<Text>().text="Combo x "+combo;
    }

    void enemyLevelUp(){

    }
}
