﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwordEnemy : Enemy
{
    // Start is called before the first frame update
    static SwordEnemy Instance;

    bool attacking;
    bool finished;

    float attack;
    float hpGrowth;
    float attackGrowth;

    void Awake(){
        Instance=this;
        isNearPlayer=false;
        hp=5;
        attack=1;

        hpGrowth=25f;
        attackGrowth=3f;

        int curLevel=Controller.getInstance().curLevel;
        hp+=hpGrowth*(float)curLevel;
        attack+=attackGrowth*(float)curLevel;

        isWalking=false;
        isFighting=false;
        isHurt=false;
        isDead=false;
        anim = GetComponent<Animator>();

        attacking=false;
        finished=false;

        anim.SetBool("isWalking",true);
        anim.SetBool("isIddle",false);
    }
    public static SwordEnemy getInstance(){
        return Instance;
    }
    // Update is called once per frame
    void Update()
    {
        attackingChecker();
        // Vector3 position = transform.position;
        // if(!this.isNearPlayer)position.x -= 2*Time.deltaTime;
        // else position.x -= 0;
        // if(hp==0){
        //     isDead=true;
        //     isFighting=false;
        //     Destroy (gameObject, anim.GetCurrentAnimatorStateInfo(0).length-0.05f); 
        // }
        // transform.position = position;
        // SetAnimationState();
    }

    void attackingChecker(){
        if(anim.GetCurrentAnimatorStateInfo(0).IsName("anim")){
            attacking=true;
            // sign=true;
            // Debug.Log("msk");
            // Controller.getInstance().playerAttack(attack);
        }
        if(!anim.GetCurrentAnimatorStateInfo(0).IsName("anim") && attacking){
            finished=true;
        }
        if(finished){
            // Debug.Log("msk");
            attacking=false;
            finished=false;
            Controller.getInstance().enemyAttack(attack);
            // Debug.Log("enemy attack");
        }
    }







}
