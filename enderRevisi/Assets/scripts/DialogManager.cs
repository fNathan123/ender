using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogManager : MonoBehaviour
{
    static DialogManager instance;
    Vector3 pos;
    GameObject bar;
    GameObject[] arrows;
    string[] arrowString;
    Vector3[] scale;
    int[] answer;


    void Awake(){
        instance=this;
        arrowString=new string[3];
        scale=new Vector3[arrowString.Length];
        // arrowString[0]="prefabs/ui/arrowsleft";
        // arrowString[1]="prefabs/ui/arrowsup";
        // arrowString[2]="prefabs/ui/arrowsright";

        arrowString[0]="prefabs/ui/sword";
        arrowString[1]="prefabs/ui/shield";
        arrowString[2]="prefabs/ui/health";

        scale[0]=new Vector3(0.642096f,0.642096f,0.642096f);
        scale[1]=new Vector3(1,1,1);
        scale[2]=new Vector3(0.1167991f,0.1167991f,0.1167991f);


        spawn2();

    }

    public static DialogManager getInstance(){
        return instance;
    }

    void Update(){
        // if(bar!=null)Debug.Log("ok");
        // else Debug.Log("not ok");
        // if(EnemyManager.getInstance().getOrderEnemySize()!=0){
        //     if(bar==null)spawn();
        //     float xEnemy = EnemyManager.getInstance().getFrontEnemy().transform.position.x;
        //     pos.x=xEnemy;
        //     bar.transform.position = pos;
        //     int change=-1;
        //     for(int i=0;i<arrows.Length;i++){
        //         Vector3 newpos=pos;
        //         newpos.x=pos.x+change;
        //         arrows[i].transform.position=newpos;
        //         change+=1;
        //     }
            
        // }

    }

    void levelUpdate(){
        System.Random rnd = new System.Random();
        Vector3 scale=new Vector3(3,3,1);
        answer=new int[arrows.Length];
        for(int i=0;i<arrows.Length;i++){
            int idx=rnd.Next(0,arrowString.Length);
            answer[i]=idx;
            arrows[i]=Instantiate(Resources.Load(arrowString[idx],typeof(GameObject))) as GameObject;
            arrows[i].transform.SetParent(this.transform);
            arrows[i].transform.localScale=scale;
            arrows[i].GetComponent<SpriteRenderer>().sortingOrder=1;
        }
    }

    void spawn(){
        bar = Instantiate(Resources.Load("prefabs/ui/dialog_box",typeof(GameObject))) as GameObject;
        bar.transform.localScale=new Vector3(0.6f,1,1);
        arrows = new GameObject[3];
        pos = bar.transform.position;
        // transform.x = x;
        pos.y = 3.2f;
        // bar.transform.position = transform;
        bar.transform.SetParent(this.transform);
        levelUpdate();
    }

    void spawn2(){
        bar = Instantiate(Resources.Load("prefabs/ui/dialog_box",typeof(GameObject))) as GameObject;
        bar.transform.localScale=new Vector3(0.6f,1,1);
        arrows = new GameObject[3];
        pos = bar.transform.position;
        // transform.x = x;
        pos.x = -8.01f;
        pos.y = 3.79f;
        bar.transform.position=pos;
        // bar.transform.position = transform;
        bar.transform.SetParent(this.transform);
        bar.GetComponent<SpriteRenderer>().sortingOrder=-1;
    }

    public void destroy(){
        // Destroy(bar);
        for(int i=0;i<arrows.Length;i++){
            Destroy(arrows[i]);
        }
        // bar=null;
    }

    public int[] getAnswer(){
        return answer;
    }
    
    public void updateDialog(int[] playerAnswer){
        answer=playerAnswer;
        for(int i=0;i<arrows.Length;i++){
            Destroy(arrows[i]);
        }
        // Vector3 scale=new Vector3(3,3,1);
        int space= -1;
        for(int i=0;i<playerAnswer.Length;i++){
            if(playerAnswer[i]!=-1){
                arrows[i]=Instantiate(Resources.Load(arrowString[playerAnswer[i]],typeof(GameObject))) as GameObject;
                arrows[i].transform.SetParent(this.transform);
                arrows[i].transform.localScale=scale[playerAnswer[i]];
                Vector3 pos2=pos;
                pos2.x+=space;
                arrows[i].transform.position=pos2;
            }
            space+=1;
        }
    }


}