﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneChanger : MonoBehaviour
{
    public void gampeplay(){
        SceneManager.LoadScene("gameplay");
    }

    public void mainMenu(){
        SceneManager.LoadScene("menu");
    }

    public void gameover(){
        SceneManager.LoadScene("gameover");
    }
}
