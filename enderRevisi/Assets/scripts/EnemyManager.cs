﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour
{
    Queue<GameObject> orderEnemy;
    // Start is called before the first frame update
    static EnemyManager instance;

    void Awake(){
        instance=this;
        orderEnemy=new Queue<GameObject>();
    }

    public static EnemyManager getInstance(){
        return instance;
    }

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Enemy[] listEnemy = GetComponentsInChildren<Enemy>();
        // Debug.Log(listEnemy[0].transform.position);
        // Debug.Log(listEnemy[0].getIsNearPlayer());
        float xAngie= Angie.getInstance().getXPosition();
        SwordEnemy temp=null;
        if(getOrderEnemySize()!=0)temp=getFrontEnemy().GetComponent<SwordEnemy>();
        else {
            Controller.getInstance().notifyPlayer();
        }
        //else notify controller
        if(temp!=null && temp.getHp()<=0){
            temp.setIsDead(true);
            temp.setIsFighting(true);
            orderEnemy.Dequeue();
            Destroy (temp.gameObject, temp.anim.GetCurrentAnimatorStateInfo(0).length-0.05f);
            Controller.getInstance().notifyDead();
        }
        foreach(Enemy enemy in listEnemy){
            Vector3 position = enemy.transform.position;
            if(Mathf.Abs(xAngie - position.x)<=2)enemy.setIsNearPlayer(true);
            if(!enemy.getIsNearPlayer())position.x -= 2*Time.deltaTime;
            else position.x -= 0;
            enemy.transform.position = position;
            enemy.SetAnimationState();
        }
    }

    public void Spawn(float x, float y){
        //CARA BIKIN OBJECT DARI PREFAB
        // GameObject swordman = Instantiate(Resources.Load("prefabs/Enemies/sword_man",typeof(GameObject))) as GameObject;
        GameObject swordman = Instantiate(Resources.Load("prefabs/Enemies/skeleton",typeof(GameObject))) as GameObject;
        Vector3 transform = swordman.transform.position;
        transform.x = x;
        transform.y = y;
        swordman.transform.position = transform;
        swordman.transform.SetParent(this.transform);
        swordman.AddComponent<SwordEnemy>();
        swordman.AddComponent<BoxCollider2D>();
        swordman.AddComponent<Rigidbody2D>();
        swordman.tag="Enemy";
        swordman.GetComponent<BoxCollider2D>().offset=new Vector2(-0.2f,0);
        swordman.GetComponent<BoxCollider2D>().size=new Vector2(0.5f,0.96f);

        orderEnemy.Enqueue(swordman);
    }

    public GameObject getFrontEnemy(){
        return orderEnemy.Peek();
    }

    public int getOrderEnemySize(){
        return orderEnemy.Count;
    }


}
