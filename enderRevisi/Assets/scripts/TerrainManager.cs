﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainManager : MonoBehaviour
{
    public float firstX;
    public float lastX;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
        MovingTerrain[] listTerrain = GetComponentsInChildren<MovingTerrain>();
        foreach(MovingTerrain terrain in listTerrain){
            // Debug.Log(terrain.transform.position);
            if(terrain.transform.position.x < firstX-1){
                Vector3 newPos = terrain.transform.position;
                newPos.x=lastX-1;
                terrain.transform.position = newPos;
            }
        }
    }
    
    public void generateTerrain(float initX,float initY){
        float x=initX;
        float y=initY;
        firstX=initX;
        for(int i=0;i<30;i++){
            Spawn(x,y);
            x+=1;
        }
        lastX=x;
    }

    public void Spawn(float x,float y){

        GameObject satuTerrain = Instantiate(Resources.Load("prefabs/terrain/tileset_19",typeof(GameObject))) as GameObject;
        Vector3 transform = satuTerrain.transform.position;
        transform.x = x;
        transform.y = y;
        satuTerrain.transform.position = transform;
        satuTerrain.transform.SetParent(this.transform);
        satuTerrain.AddComponent<MovingTerrain>();
        satuTerrain.AddComponent<BoxCollider2D>();
        
    }
}
